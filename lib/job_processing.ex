defmodule JobProcessing do
  @moduledoc """
  sorting dependencied using topological sort from elixir's libgraph
  """
  def sort_tasks(vertices, edges) do
    g = Graph.new |> Graph.add_vertices(vertices)
    g = Graph.add_edges(g, edges)
    Graph.topsort(g)
    |> Enum.reverse()
  end
end
