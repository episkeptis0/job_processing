defmodule JobProcessingWeb.JobController do
  use JobProcessingWeb, :controller

  def sort(conn, %{"tasks" => tasks}) when is_list(tasks) do
    conn
    |> render(sorted_tasks: sorted_tasks_map(tasks))
  end

  def shell(conn, %{"tasks" => tasks}) when is_list(tasks) do
    conn
    |> text(to_bash_script(tasks))
  end

  def shell(conn, params) do
    shell(conn, decode_data(params))
  end

  defp decode_data(params) do
    Map.keys(params) |> List.first() |> Jason.decode!()
  end

  def get_vertices(tasks) do
    IO.inspect(tasks)
    tasks
      |> Enum.map(fn x -> Map.get(x, "name") end)
  end

  def get_edges(tasks) do
    tasks
      |> Enum.map(fn x -> {Map.get(x, "name"), Map.get(x, "requires")} end)
      |> Enum.filter(fn x -> elem(x, 1) != nil end)
      |> Enum.map(fn x-> Enum.map(elem(x, 1), fn y -> {elem(x, 0), y} end) end)
      |> List.flatten()
  end

  def get_name_commands_map(tasks) do
    tasks
      |> Enum.map(fn x ->  {Map.get(x, "name"), Map.get(x, "command")} end)
      |> Enum.into(%{})
  end

  #add guards
  def sorted_tasks_list(tasks) do
    JobProcessing.sort_tasks(get_vertices(tasks), get_edges(tasks))
  end

  def sorted_tasks_map(tasks) do
    names_commands = get_name_commands_map(tasks)
    new_tasks = %{name: "task", command: "command"}
    Enum.map(sorted_tasks_list(tasks), fn x -> %{new_tasks | name: x, command: Map.get(names_commands, x)} end)
  end

  def to_bash_script(tasks) do
    commands =
      sorted_tasks_map(tasks)
        |> Enum.map(fn x -> Map.get(x, :command) end)
        |> Enum.join("\n")
    "#!/usr/bin/env bash\n\n#{commands}\n"
  end
end
