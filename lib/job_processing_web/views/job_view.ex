defmodule JobProcessingWeb.JobView do
  use JobProcessingWeb, :view

  def render("sort.json", %{sorted_tasks: sorted_tasks}) do
    %{sorted_tasks: sorted_tasks}
  end

  def render("shell.text", sorted_tasks) do
    sorted_tasks
  end

end
