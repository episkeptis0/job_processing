defmodule JobProcessingWeb.Router do
  use JobProcessingWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api/v1", JobProcessingWeb do
    pipe_through :api

    post "/sort",  JobController, :sort
    post "/shell", JobController, :shell
  end
end
