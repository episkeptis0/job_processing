#!/bin/sh
set -e

# Ensure the app's dependencies are installed
mix deps.compile
mix deps.get

echo "\nTesting the installation..."
# "Proove" that install was successful by running the tests
mix test

echo "\n Launching Phoenix web server..."
# Start the phoenix web server
mix phx.server