# Job Processing

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Docker

Make sure to have `docker-compose` installed

```shell
make start
```

## Test

#### API

```shell
curl -d @fixtures/mytasks.json http://localhost:4000/api/v1/shell | zsh
```

#### Shell

```shell
curl -X POST \
  http://localhost:4000/api/v1/sort \
  -H 'Content-Type: application/json' \
  -d '{
        "tasks": [{
                "name": "task-1",
                "command": "touch /tmp/file1"
        }, {
                "name": "task-2",
                "command": "cat /tmp/file1",
                "requires": ["task-3"]
        }, {
                "name": "task-3",
                "command": "echo `Hello World!` > /tmp/file1",
                "requires": ["task-1"]
        }, {
                "name": "task-4",
                "command": "rm /tmp/file1",
                "requires": ["task-2", "task-3"]
        }]
}'
```

## TODO
- handle errors
- add guards
- add unit tests
- add logging
- multistage docker build
  

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
