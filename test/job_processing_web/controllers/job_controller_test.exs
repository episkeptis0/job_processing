defmodule JobProcessingWeb.JobControllerTest do
  use JobProcessingWeb.ConnCase

  test "returns the tasks sorted based on it's dependencies" do
    data = %{:tasks =>[
                  %{:name => "task-1",
                    :command => "touch /tmp/file1"},
                  %{:name => "task-2",
                    :command => "cat /tmp/file1",                   :requires => ["task-3"]},
                  %{:name => "task-3",
                    :command => "echo `Hello World!` > /tmp/file1", :requires => ["task-1"]},
                  %{:name => "task-4",
                    :command => "rm /tmp/file1",                    :requires => ["task-2", "task-3"]}
               ]
              }

    response =
      build_conn()
      |> post("/api/v1/sort", data)
      |> json_response(200)

    assert %{
            "sorted_tasks" => [%{
                              "command" => "touch /tmp/file1",
                              "name"    => "task-1"
                            },
                            %{
                              "command" => "echo `Hello World!` > /tmp/file1",
                              "name"    => "task-3"
                            },
                            %{
                              "command" => "cat /tmp/file1",
                              "name"    => "task-2"
                            },
                            %{
                              "command" => "rm /tmp/file1",
                              "name"    => "task-4"
                            }
            ]
          } = response
    # true = true
  end
end
